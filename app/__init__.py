import mongoengine
from flask import Flask

app = Flask(__name__)
mongoengine.connect(host="127.0.0.1")

from . import routes, models
