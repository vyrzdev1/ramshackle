import mongoengine
from . import app


class Container(mongoengine.Document):
    parent = mongoengine.ReferenceField("self")

    name = mongoengine.StringField()
    description = mongoengine.StringField()
    thumbnail = mongoengine.FileField()
    URLName = mongoengine.StringField()


class Product(mongoengine.Document):
    container = mongoengine.ReferenceField(Container)

    # Public
    name = mongoengine.StringField()
    URLName = mongoengine.StringField()
    description = mongoengine.StringField()
    thumbnail = mongoengine.FileField()
    price = mongoengine.DictField()
    tags = mongoengine.ListField()

    # Private ~
    purchaseArrangement = mongoengine.StringField()
    ourCost = mongoengine.DictField()
    payoutSplit = mongoengine.StringField()
    reservePrice = mongoengine.DictField()
    ownerContact = mongoengine.StringField()
