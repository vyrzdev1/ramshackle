from . import app, models
from flask import render_template


@app.route("/")
def index():
    return render_template("public/index.html")


@app.route("/catalog/<path:path>")
def catalog(path):
    # Split the path;
    splitPath = path.split("/")
    currentObj = models.Container.objects(URLName=splitPath[0]).first()
    splitPath.pop(0)
    print(splitPath)
    for URLName in splitPath:
        currentObj = models.Container.objects(URLName=URLName, parent=currentObj).first()
    print(currentObj)
    cats = models.Container.objects(parent=currentObj)
    products = models.Product.objects(container=currentObj)

    return render_template("public/catalog_view.html", categories=cats, products=products)


@app.route("/product/<productID>")
def product(productID):
    pass
